package info.zamiq.bookmanagementms.service;

import info.zamiq.bookmanagementms.dto.BookDto;

public interface BookService {
    BookDto createBook(BookDto bookDto);

    BookDto findAll();

    void delete(Long id);
}
