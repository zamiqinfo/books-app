package info.zamiq.bookmanagementms.service.impl;

import info.zamiq.bookmanagementms.domain.Book;
import info.zamiq.bookmanagementms.dto.BookDto;
import info.zamiq.bookmanagementms.repository.BookRepository;
import info.zamiq.bookmanagementms.service.BookService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class BookServiceImpl implements BookService {

    private final BookRepository bookRepository;
    private final ModelMapper modelMapper;

    @Override
    public BookDto createBook(BookDto bookDto) {
        Book book = modelMapper.map(bookDto, Book.class);
        return modelMapper.map(bookRepository.save(book), BookDto.class);
    }

    @Override
    public BookDto findAll() {
        return modelMapper.map(bookRepository.findAll(), BookDto.class);
    }

    @Override
    public void delete(Long id) {
        Optional<Book> book=bookRepository.findById(id);
        bookRepository.delete(book.get());
    }
}
