package info.zamiq.bookmanagementms.repository;

import info.zamiq.bookmanagementms.domain.Book;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookRepository extends JpaRepository<Book,Long> {
}
