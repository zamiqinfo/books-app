package info.zamiq.bookmanagementms.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@ApiModel(description = "Book information")
public class BookDto {
    @ApiModelProperty(notes = "Kitab adi")
    @NotNull(message = "Kitab adi boş ola bilməz")
    @NotBlank(message = "Kitab adi boş ola bilməz")
    String name;

    @ApiModelProperty(notes = "Qısa xülasə")
    String description;
}
