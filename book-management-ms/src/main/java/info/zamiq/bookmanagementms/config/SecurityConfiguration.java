package info.zamiq.bookmanagementms.config;

import info.zamiq.usermanagementms.jwt.JwtConfigurer;
import info.zamiq.usermanagementms.jwt.JwtTokenFilter;
import info.zamiq.usermanagementms.security.SecurityConfig;
import info.zamiq.usermanagementms.security.handler.CustomAccessDeniedHandler;
import info.zamiq.usermanagementms.security.handler.IntegAuthenticationEntryPoint;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;

@Slf4j
@Configuration
@EnableWebSecurity
@Import({CustomAccessDeniedHandler.class, JwtConfigurer.class, IntegAuthenticationEntryPoint.class})
public class SecurityConfiguration extends SecurityConfig {

    private static final String BOOK_API_V1 = "api/v1/book/**";

    @Bean
    public UserDetailsService userDetailsService() {
        return super.userDetailsService();
    }
//    @Autowired
//    JwtTokenFilter jwtTokenFilter;

    public SecurityConfiguration(JwtConfigurer jwtConfigurer, IntegAuthenticationEntryPoint jwtAuthenticationEntryPoint, CustomAccessDeniedHandler jwtAccessDeniedHandler, JwtTokenFilter jwtTokenFilter) {
        super(jwtConfigurer, jwtAuthenticationEntryPoint, jwtAccessDeniedHandler);

    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers(HttpMethod.GET, BOOK_API_V1).permitAll()
                .antMatchers(HttpMethod.POST, BOOK_API_V1).permitAll();

        super.configure(http);
    }

}
