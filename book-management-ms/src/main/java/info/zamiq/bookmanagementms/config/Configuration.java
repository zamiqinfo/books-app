package info.zamiq.bookmanagementms.config;

import info.zamiq.usermanagementms.jwt.JwtTokenProvider;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.userdetails.UserDetailsService;

@org.springframework.context.annotation.Configuration
@RequiredArgsConstructor
public class Configuration {

    private final UserDetailsService userDetailsService;

    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }

    @Bean
    public JwtTokenProvider jwtTokenProvider(){
        return new JwtTokenProvider(userDetailsService);
    }
}
