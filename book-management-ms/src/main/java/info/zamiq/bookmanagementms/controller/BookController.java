package info.zamiq.bookmanagementms.controller;

import info.zamiq.bookmanagementms.dto.BookDto;
import info.zamiq.bookmanagementms.service.BookService;
import info.zamiq.usermanagementms.jwt.JwtTokenProvider;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

@RestController
@RequestMapping("api/v1/book")
@RequiredArgsConstructor
public class BookController {

    private final BookService bookService;
    private final JwtTokenProvider jwtTokenProvider;

    @PostMapping("/create")
    public BookDto create(@Valid @RequestBody BookDto bookDto, HttpServletRequest request, HttpServletResponse response){
        String token=request.getHeader("Bearer");
        jwtTokenProvider.isValidToken(token);
        Authentication auth= jwtTokenProvider.getAuthentication(token);
        SecurityContextHolder.getContext().setAuthentication(auth);

        return bookService.createBook(bookDto);
    }

    @GetMapping("/findAll")
    public BookDto findAll(){
        return bookService.findAll();
    }

    @PostMapping("/delete")
    public void delete(Long id){
        bookService.delete(id);
    }
}
