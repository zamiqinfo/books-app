package info.zamiq.usermanagementms.repository;

import info.zamiq.usermanagementms.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.Optional;

public interface UserReposiroty extends JpaRepository<User,Long> {
    Optional<User> findByUsername(String user);
}
