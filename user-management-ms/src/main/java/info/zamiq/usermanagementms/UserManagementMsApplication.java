package info.zamiq.usermanagementms;


import info.zamiq.usermanagementms.domain.User;
import info.zamiq.usermanagementms.jwt.JwtTokenProvider;
import info.zamiq.usermanagementms.repository.UserReposiroty;
import info.zamiq.usermanagementms.security.user.UserRole;
import info.zamiq.usermanagementms.security.user.UserStatus;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import lombok.Builder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@SpringBootApplication
@Slf4j
@Builder
public class UserManagementMsApplication implements CommandLineRunner {

	private final UserReposiroty userReposiroty;
	private final JwtTokenProvider jwtTokenProvider;

	public static void main(String[] args) {
		SpringApplication.run(UserManagementMsApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
//		log.info("Creating default PUBLISHER");
//
//		User user1=User.builder()
//				.username("Zamiq")
//				.password(jwtTokenProvider.passwordEncoder().encode("123456"))
//				.status(UserStatus.ACTIVE)
//				.role(UserRole.PUBLISHER)
//				.build();
//
//		String token = jwtTokenProvider.createToken(user1.getUsername(), user1.getRole().name());
//		user1.setToken(token);
//		log.info("JWT token is: {}",token);
//		userReposiroty.save(user1);
	}
}
