package info.zamiq.usermanagementms.dto;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@ApiModel(description = "User information")
public class UserAuthenticationRequestDTO {

    @ApiModelProperty(notes = "istifadəçi adi")
    @NotNull(message = "username boş ola bilməz")
    @NotBlank(message = "username boş ola bilməz")
    private String username;

    @ApiModelProperty(notes = "istifadəçi password")
    @NotNull(message = "password boş ola bilməz")
    @NotBlank(message = "password boş ola bilməz")
    private String password;
}

