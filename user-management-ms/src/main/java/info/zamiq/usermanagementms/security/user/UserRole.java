package info.zamiq.usermanagementms.security.user;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public enum UserRole {
    READER(new HashSet<>(Arrays.asList(UserPermission.READ))),
    PUBLISHER(new HashSet<>(Arrays.asList(UserPermission.READ, UserPermission.WRITE, UserPermission.CREATE,
            UserPermission.EDIT, UserPermission.UPDATE, UserPermission.DELETE)));


    private Set<UserPermission> permissions;

    UserRole(Set<UserPermission> permissions) {
        this.permissions = permissions;
    }

    public Set<UserPermission> getPermissions() {
        return permissions;
    }

    public Set<SimpleGrantedAuthority> getAuthorities() {
        return getPermissions().stream()
                .map(permission -> new SimpleGrantedAuthority(permission.getPermission()))
                .collect(Collectors.toSet());
    }

}
