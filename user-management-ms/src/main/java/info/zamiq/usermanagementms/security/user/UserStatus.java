package info.zamiq.usermanagementms.security.user;

public enum UserStatus {
    ACTIVE, BANNED
}
