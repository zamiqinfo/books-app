package info.zamiq.usermanagementms.security.handler;

import com.fasterxml.jackson.databind.ObjectMapper;
import info.zamiq.usermanagementms.security.exception.ApiError;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;

@Component("IntegAuthenticationEntryPoint")
public class IntegAuthenticationEntryPoint implements AuthenticationEntryPoint {
    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authenticationException) throws IOException, ServletException {

        ApiError responseApi = new ApiError(40401, "Access Denied, Invalidate token");

        response.setContentType("application/json");
        OutputStream out = response.getOutputStream();
        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(out, responseApi);
        out.flush();
    }
}
