package info.zamiq.usermanagementms.security.user;

public enum UserPermission {
    READ("read"),
    WRITE("write"),
    UPDATE("update"),
    EDIT("edit"),
    CREATE("create"),
    DELETE("delete");

    private final String permission;

    UserPermission(String permission) {
        this.permission = permission;
    }

    public String getPermission() {
        return permission;
    }
}
