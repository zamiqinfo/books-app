package info.zamiq.usermanagementms.security.exception;

import lombok.Getter;
import org.springframework.security.core.AuthenticationException;

@Getter
public class JwtAuthenticationException extends AuthenticationException {
    private int code;
    private String message;
    public JwtAuthenticationException(String msg) {
        super(msg);
    }
    public JwtAuthenticationException(int code, String message) {
        super(message);
        this.code=code;
        this.message=message;
    }

}
