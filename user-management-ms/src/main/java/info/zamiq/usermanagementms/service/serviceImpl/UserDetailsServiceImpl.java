package info.zamiq.usermanagementms.service.serviceImpl;

import info.zamiq.usermanagementms.domain.User;
import info.zamiq.usermanagementms.repository.UserReposiroty;
import info.zamiq.usermanagementms.security.user.SecurityUser;
import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

@Component("userDetailsServiceImpl")
@AllArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {

    UserReposiroty userReposiroty;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user=userReposiroty.findByUsername(username).get();
        return SecurityUser.fromUser(user);
    }
}
