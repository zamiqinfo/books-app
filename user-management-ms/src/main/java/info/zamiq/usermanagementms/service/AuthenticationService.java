package info.zamiq.usermanagementms.service;


import info.zamiq.usermanagementms.dto.UserAuthenticationRequestDTO;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface AuthenticationService {
    ResponseEntity<?> authenticate(UserAuthenticationRequestDTO requestBody, HttpServletRequest request, HttpServletResponse response);
}
