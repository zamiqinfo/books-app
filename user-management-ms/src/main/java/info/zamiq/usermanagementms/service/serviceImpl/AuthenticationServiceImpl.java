package info.zamiq.usermanagementms.service.serviceImpl;

import info.zamiq.usermanagementms.jwt.JwtTokenProvider;
import info.zamiq.usermanagementms.domain.User;
import info.zamiq.usermanagementms.dto.UserAuthenticationRequestDTO;
import info.zamiq.usermanagementms.repository.UserReposiroty;
import info.zamiq.usermanagementms.security.user.UserRole;
import info.zamiq.usermanagementms.security.user.UserStatus;
import info.zamiq.usermanagementms.service.AuthenticationService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Component
@AllArgsConstructor
@Slf4j
public class AuthenticationServiceImpl implements AuthenticationService {

    private final AuthenticationManager authenticationManager;

    private final UserReposiroty userReposiroty;
    private final JwtTokenProvider jwtTokenProvider;

    @Override
    public ResponseEntity<?> authenticate(@RequestBody UserAuthenticationRequestDTO requestBody, HttpServletRequest request, HttpServletResponse response) {
        Map<Object, Object> responseBody = new HashMap<>();
        try {
            Optional<User> user= userReposiroty.findByUsername(requestBody.getUsername());
            String token ="";
            if(user!=null && !user.isEmpty()){
                authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(requestBody.getUsername(), requestBody.getPassword()));
                token = jwtTokenProvider.createToken(requestBody.getUsername(), user.get().getRole().name());
                log.info("DB JWT token is: {}",token);
                User dbUser=user.get();
                dbUser.setToken(token);
                flushCache();
            }else{
                log.info("Creating default READER");
                User newUser=User.builder()
                        .username(requestBody.getUsername())
                        .password(jwtTokenProvider.passwordEncoder().encode(requestBody.getPassword()))
                        .status(UserStatus.ACTIVE)
                        .role(UserRole.READER)
                        .build();
//                authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(newUser.getUsername(), newUser.getPassword()));
                token = jwtTokenProvider.createToken(newUser.getUsername(), newUser.getRole().name());
                newUser.setToken(token);
                log.info("New JWT token is: {}",token);
                userReposiroty.save(newUser);
            }
            responseBody.put("username", requestBody.getUsername());
            responseBody.put("token", token);

            return ResponseEntity.ok(responseBody);
        } catch (AuthenticationException e) {
            e.printStackTrace();
            return new ResponseEntity<>("{\"code\":40404,\"message\":\"Invalid email/password combination\"},", HttpStatus.FORBIDDEN);
        }
    }

    @CacheEvict(cacheNames="token", allEntries=true)
    public void flushCache() { }
}
