package info.zamiq.usermanagementms.controller;


import info.zamiq.usermanagementms.dto.UserAuthenticationRequestDTO;
import info.zamiq.usermanagementms.security.exception.ApiError;
import info.zamiq.usermanagementms.service.AuthenticationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/auth")
@AllArgsConstructor
@Api(value = "Login System", description = "Generate token")
public class AuthenticationController {

    private final AuthenticationService authenticationService;

    @ApiOperation(value = "Create token")
    @PostMapping("/login")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 401, message =
                    "{\"code\":40101,\"message\":\"Sistemdə xəta baş verdi!\"}," +
                    "{\"code\":40102,\"message\":\"Invalid email/password combination\"},", response = ApiError.class)
    })
    public ResponseEntity<?> authenticate(@RequestBody UserAuthenticationRequestDTO requestBody, HttpServletRequest request, HttpServletResponse response) {
        return authenticationService.authenticate(requestBody, request, response);
    }

}
