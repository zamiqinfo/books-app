package info.zamiq.usermanagementms.controller;

import info.zamiq.usermanagementms.dto.UserAuthenticationRequestDTO;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/test")
public class TestController {
    @GetMapping("/sw")
    public UserAuthenticationRequestDTO testSW(UserAuthenticationRequestDTO us){
        UserAuthenticationRequestDTO sw=new UserAuthenticationRequestDTO();
        sw.setUsername("Zamiq");
        return sw;
    }

    @GetMapping("/sw1")
    public String test1(){
        return "Hello guys";
    }
}
